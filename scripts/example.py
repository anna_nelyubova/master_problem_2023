#!/usr/bin/env python3

import rospy

from trajectory_msgs.msg import JointTrajectory
from trajectory_msgs.msg import JointTrajectoryPoint
from std_msgs.msg import Duration

from std_msgs.msg import Bool
from std_srvs.srv import SetBool

from geometry_msgs.msg import Twist
from geometry_msgs.msg import Vector3


class Example(object):

    def __init__(self) -> None:
        rospy.loginfo("[Example] loaging")

        # arm init
        self.arm_traj_pub = rospy.Publisher(
            '/ceiling_robot/arm/scaled_pos_traj_controller/command', JointTrajectory, queue_size=10, latch=True)
        self.ceiling_traj_pub = rospy.Publisher(
            '/ceiling_robot/ceiling_base_controller/command', JointTrajectory, queue_size=10, latch=True)


        # gripper init
        rospy.wait_for_service('/gripper/grasp')
        self.grasp_srv = rospy.ServiceProxy('/gripper/grasp', SetBool)

        self.ready_gripper_sub = rospy.Subscriber(
            "/gripper/ready", Bool, self.ready_callback)
        self.state_gripper_sub = rospy.Subscriber(
            "/gripper/state", Bool, self.state_callback)

        self.ready_gripper = False
        self.state_gripper = False

        self.robot_a_cmd_vel_pub = rospy.Publisher('/robot_a/cmd_vel', Twist, queue_size=10, latch=True)
        self.robot_b_cmd_vel_pub = rospy.Publisher('/robot_b/cmd_vel', Twist, queue_size=10, latch=True)

        rospy.on_shutdown(self.shutdown)

    def __del__(self) -> None:
        # remove and close
        pass

    def shutdown(self):
        # stop robots here
        self.robot_a_cmd_vel_pub.publish(Twist())
        self.robot_b_cmd_vel_pub.publish(Twist())
        # ...
        pass

    def ready_callback(self, msg):
        self.ready_gripper = msg.data

    def state_callback(self, msg):
        self.state_gripper = msg.data

    def arm_move_to(self, q, theta):
        N = 6
        arm_joints = ["arm_shoulder_pan_joint",
                      "arm_shoulder_lift_joint",
                      "arm_elbow_joint",
                      "arm_wrist_1_joint",
                      "arm_wrist_2_joint",
                      "arm_wrist_3_joint"]
        trajectory = JointTrajectory()
        trajectory.points.append(JointTrajectoryPoint())
        d = Duration()
        d.data.secs = 1
        trajectory.points[0].time_from_start = d.data
        for i in range(N):
            trajectory.joint_names.append(arm_joints[i])
            trajectory.points[0].positions.append(q[i])
            trajectory.points[0].velocities.append(0)
            trajectory.points[0].accelerations.append(0)
            trajectory.points[0].effort.append(0)
        self.arm_traj_pub.publish(trajectory)

        joints = ["long_joint", "short_joint"]
        trajectory2 = JointTrajectory()
        trajectory2.points.append(JointTrajectoryPoint())
        d = Duration()
        d.data.secs = 1
        trajectory2.points[0].time_from_start = d.data
        for i in range(2):
            trajectory2.joint_names.append(joints[i])
            trajectory2.points[0].positions.append(theta[i])
            trajectory2.points[0].velocities.append(0)
            trajectory2.points[0].accelerations.append(0)
            trajectory2.points[0].effort.append(0)
        self.ceiling_traj_pub.publish(trajectory2)


    def spin(self):
        # set some arm configuration in JS

        cr1 = [0, 3]
        ur1 = [0.000150, -0.135619, -0.691150,  -1.005309, 1.382301, 0.251327]

        cr2 = [0, 0]
        ur2 = [1.005310, -0.0, 0.816814,  -1.005308, 1.002301, 0.251327]

        flag = True
        can_catch = False
        time_to_catch = 99999999

        rate = rospy.Rate(30)
        t0 = rospy.get_time()
        while not rospy.is_shutdown():
            t = rospy.get_time() - t0
            # move arm to a grasping object
            self.arm_move_to(ur1, cr1)
                        
            # try to grasp it
            if self.ready_gripper: # if gripper ready to a grasp object (there is collision between the gripper and an object)
                rospy.loginfo("try to grasp")
                try:
                    resp = self.grasp_srv(True)
                    rospy.loginfo(resp.message)
                except rospy.ServiceException as e:
                    print("Service call failed: %s" % e)

            if self.state_gripper:  # if gripper has an object (an object connected to the gripper)
                rospy.loginfo("run away")                
                # lift the object
                self.arm_move_to(ur2, cr2)
                if flag:
                    time_to_catch = rospy.get_time()
                    flag = not flag
                    
                # wait a little bit
                if rospy.get_time() - time_to_catch > 5:
                    can_catch = True
                
                # catch the object
                if can_catch:
                    rospy.loginfo("catcj the onject")                
                    try:
                        resp = self.grasp_srv(False)
                        rospy.loginfo(resp.message)
                    except rospy.ServiceException as e:
                        print("Service call failed: %s" % e)
            
            # move mobile robot
            self.robot_a_cmd_vel_pub.publish(Twist(Vector3(), Vector3(0,0,0.1)))

            rate.sleep()


def main(args=None):
    rospy.init_node('example_node')

    exp = Example()
    exp.spin()


if __name__ == '__main__':
    main()
